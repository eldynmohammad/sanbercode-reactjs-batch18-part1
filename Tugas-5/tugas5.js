// Soal 1
console.log("----------------------------------------Soal 1");
function halo() {
  return "Halo Sanbers!";
}

console.log(halo());

// Soal 2
console.log("----------------------------------------Soal 2");
var num1 = 12;
var num2 = 4;

function kalikan() {
  return num1 * num2;
}
var hasilKali = kalikan(num1, num2);
console.log(hasilKali); // 48

// Soal 3
console.log("----------------------------------------Soal 3");
function introduce() {
  return (
    "Nama saya " +
    name +
    ", umur saya " +
    age +
    " tahun, alamat saya di " +
    address +
    ", dan saya punya hobby yaitu " +
    hobby +
    "!"
  );
}
var name = "John";
var age = 30;
var address = "Jalan belum jadi";
var hobby = "Gaming";

var perkenalan = introduce(name, age, address, hobby);
console.log(perkenalan); //// Menampilkan "Nama saya John, umur saya 30 tahun, alamat saya di jalan belum jadi, dan saya punya hobby yaitu Gaming!"

// Soal 4
console.log("----------------------------------------Soal 4");
var arrayDaftarPeserta = [
  {
    nama: "Asep",
    "jenis kelamin": "laki-laki",
    hobby: "baca buku",
    "tahun lahir": 1992,
  },
];
console.log(arrayDaftarPeserta);
//Gara-gara prettier "nama" jadi nama, ga bisa dikasih quotation, dan juga hobby

// Soal 5
console.log("----------------------------------------Soal 5");
var nama_buah = [
  { nama: "strawberry", warna: "merah", "ada bijinya": "tidak", harga: 9000 },
  { nama: "jeruk", warna: "oranye", "ada bijinya": "ada", harga: 8000 },
  {
    nama: "Semangka",
    warna: " Hijau & Merah",
    "ada bijinya": "ada",
    harga: 10000,
  },
  { nama: "Pisang", warna: "Kuning", "ada bijinya": "tidak", harga: 5000 },
];
console.log(nama_buah[0]);
//Gara-gara prettier ga bisa kasih quotation
