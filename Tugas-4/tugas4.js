// Soal 1
var angka = 2;
console.log("LOOPING PERTAMA");
while (angka < 22) {
  console.log(angka + " - I love coding");
  angka = angka + 2;
}
console.log("LOOPING KEDUA");
var angka2 = 20;
while (angka2 > 0) {
  console.log(angka2 + " - I will become a frontend developer");
  angka2 = angka2 - 2;
}

// Soal 2
for (var angka = 1; angka <= 20; angka++) {
  if (angka % 2 == 0) {
    console.log(angka + " - Berkualitas");
  } else if (angka % 2 == 1) {
    if (angka % 3 == 0) {
      console.log(angka + " - I love Coding");
    } else {
      console.log(angka + " - Santai");
    }
  }
}

// Soal 4
var kalimat = "saya sangat senang belajar javascript";
var pecah = kalimat.split(" ");
console.log(pecah);
